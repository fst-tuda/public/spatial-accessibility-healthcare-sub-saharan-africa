# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:43:18 2024

@author: Friesen
"""
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 27 15:49:06 2023

@author: Friesen
"""

import pandas as pd
import matplotlib.pyplot as plt

# Read the total population data for each city
hospital_code = 'C0'
time = '15'
df_total_population = pd.read_csv(f'Total_Population_{hospital_code}.csv')
city_population = df_total_population.groupby('City')['Total_Population'].sum().reset_index()

# Load specified time data for Formal and Informal areas
file1 = f'specified_time_data_{hospital_code}_Formal_{time}_min.csv'
file2 = f'specified_time_data_{hospital_code}_Informal_{time}_min.csv'
df_formal = pd.read_csv(file1)
df_informal = pd.read_csv(file2)

# Merge the two DataFrames on the 'City' column
merged_df = pd.merge(df_formal, df_informal, on='City', suffixes=('_Formal', '_Informal'))

# Calculate the ratio of normalized populations and uncertainty intervals
merged_df['Normalized_Population_Ratio'] = merged_df['Normalized_Population_Formal'] / merged_df['Normalized_Population_Informal']
merged_df['Population_upper_ratio'] = merged_df['Population_Upper_Formal'] / merged_df['Population_Lower_Informal']
merged_df['Population_lower_ratio'] = merged_df['Population_Lower_Formal'] / merged_df['Population_Upper_Informal']

# Save the result to a new CSV file
result_file = f'population_ratio_with_uncertainty_{time}_.csv'
merged_df.to_csv(result_file, index=False)

# Print the first few rows of the result
print(merged_df.head())

# Create a figure and axis for the plot
fig, ax = plt.subplots(figsize=(6, 6))

# Iterate over cities in the merged DataFrame
for i, city in enumerate(merged_df['City']):
    city_data = merged_df.loc[merged_df['City'] == city]

    # Extract relevant data for the current city
    normalized_population = city_data['Normalized_Population_Ratio'].values[0]
    lower = city_data['Population_lower_ratio'].values[0]
    upper = city_data['Population_upper_ratio'].values[0]

    # Plot the point for normalized population at the time of interest
    plt.plot(normalized_population, i, 's', markersize=8, label=city)

    # Plot uncertainty bars
    plt.hlines(i, lower, upper, color='gray', lw=2)

# Customize the plot
plt.axvline(x=1, color='black', linestyle='--')  # Plot a dashed line at the relative risk value 1
plt.yticks(range(len(merged_df['City'])), merged_df['City'])
plt.xlabel(f'Relative Risk at Time {time} mins')
plt.title(f'Normalized Population Ratio at Time {time} mins with Uncertainty')
plt.xlim(0.1, 10)  # Adjust the x-axis limits as needed
plt.tight_layout()
plt.xscale('log')

# Show the plot
plt.show()

# Merge city_population DataFrame with the result DataFrame for each time
analysis_df = pd.merge(city_population, merged_df, on='City')

# List of times
times = ['15', '30', '60']

# Create a figure and axis
fig, ax = plt.subplots(figsize=(10, 4))

# Iterate over each time
for time in times:
    # Load the CSV file for the current time
    file = f'population_ratio_with_uncertainty_{time}_.csv'
    analysis_df = pd.read_csv(file)

    # Merge with city_population DataFrame to get 'Total_Population'
    analysis_df = pd.merge(city_population, analysis_df, on='City')

    # Scatter plot with different markers for each time
    ax.scatter(analysis_df['Total_Population'],
               analysis_df['Normalized_Population_Ratio'],
               label=f'{time} mins',
               alpha=0.7)

# Adding horizontal line at relative risk value 1
ax.axhline(y=1, color='black', linestyle='--')

# Adding vertical lines for population thresholds
ax.axvline(x=250000, color='black', linestyle='--')
ax.axvline(x=1000000, color='black', linestyle='--')
ax.axvline(x=5000000, color='black', linestyle='--')

# Adding text boxes between vertical lines
ax.text(130000, 0.3, 'Small Cities', rotation=0, verticalalignment='center')
ax.text(300000, 0.3, 'Medium-Sized Cities', rotation=0, verticalalignment='center')
ax.text(1600000, 0.3, 'Large Cities', rotation=0, verticalalignment='center')
ax.text(6000000, 0.3, 'Very Large Cities', rotation=0, verticalalignment='center')

# Setting x-axis and y-axis to log scale
ax.set_xscale('log')
ax.set_yscale('log')
plt.ylim(0.1, 10)

# Adding labels and title
ax.set_xlabel('Population')
ax.set_ylabel('Relative Risk')
ax.legend()

# Display the plot
plt.tight_layout()
plt.show()
