# -*- coding: utf-8 -*-
"""
Created on Mon Jan 22 11:43:34 2024

@author: Friesen
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# Parameters
uncertainty_factor = 0.2
time_of_interest = 60
hospital_code = 'C0'

# Load data from CSV files
time_data = pd.read_csv(f'combined_data_{hospital_code}.csv')
population_data = pd.read_csv(f'Total_Population_{hospital_code}.csv').drop('Time', axis=1)

# Merge data based on 'City' and 'Landuse'
merged_data = pd.merge(time_data, population_data, on=['City', 'Landuse'])

# Group by 'City' and 'Time', calculate total population and normalize
total_df = merged_data.groupby(['City', 'Time'], as_index=False).agg({
    'Total_Population_x': 'sum',
    'Total_Population_y': 'sum'
})
total_df['Normalized'] = total_df['Total_Population_x'] / total_df['Total_Population_y']

# Plot total population over time for all cities
fig, ax = plt.subplots(figsize=(10, 6))
cities = total_df['City'].unique()
for i, city in enumerate(cities):
    linestyle = '-' if i < 9 else '--' if i < 17 else ':'
    city_data = total_df[total_df['City'] == city]

    city_data['time_lower'] = city_data['Time'] - uncertainty_factor * city_data['Time']
    city_data['time_upper'] = city_data['Time'] + uncertainty_factor * city_data['Time']

    city_data['population_lower'] = np.interp(city_data['time_lower'], city_data['Time'], city_data['Normalized'])
    city_data['population_upper'] = np.interp(city_data['time_upper'], city_data['Time'], city_data['Normalized'])

    ax.plot(city_data['Time'], city_data['Normalized'], label=f'{city}', linestyle=linestyle)
    plt.fill_between(city_data['Time'], city_data['population_lower'], city_data['population_upper'], alpha=0.2)

# Add labels and legend
ax.set_xlabel('Time')
ax.set_ylabel('Total Population')
ax.set_title('Total Population Over Time for All Cities')
ax.legend(loc='upper left', bbox_to_anchor=(1, 1))
plt.ylim(0, 1)
plt.xlim(0, 120)
plt.show()

# Create a dataframe to store normalized population and uncertainties for each city at a specific timestep
result_df = pd.DataFrame(columns=['City', 'Normalized_Population', 'Lower_Uncertainty', 'Upper_Uncertainty'])

for city in cities:
    city_data = total_df[total_df['City'] == city]

    time_lower = time_of_interest - uncertainty_factor * time_of_interest
    time_upper = time_of_interest + uncertainty_factor * time_of_interest

    normalized_lower = np.interp(time_lower, city_data['Time'], city_data['Normalized']).item()
    normalized_upper = np.interp(time_upper, city_data['Time'], city_data['Normalized']).item()

    result_df = result_df.append({
        'City': city,
        'Normalized_Population': np.interp(time_of_interest, city_data['Time'], city_data['Normalized']).item(),
        'Lower_Uncertainty': float(normalized_lower),
        'Upper_Uncertainty': float(normalized_upper)
    }, ignore_index=True)

# Display the resulting dataframe
print(result_df)

# Sort the dataframe based on 'Normalized_Population'
result_df_sorted = result_df.sort_values(by='Normalized_Population', ascending=True)

# Plot normalized population for each city with uncertainties
fig, ax = plt.subplots(figsize=(6, 6))
ax.errorbar(result_df_sorted['Normalized_Population'], range(len(result_df_sorted)),
            xerr=[result_df_sorted['Normalized_Population'] - result_df_sorted['Lower_Uncertainty'],
                  result_df_sorted['Upper_Uncertainty'] - result_df_sorted['Normalized_Population']],
            fmt='s', markersize=8, color='black', capsize=5)

# Add labels and title
ax.set_yticks(range(len(result_df_sorted)))
ax.set_yticklabels(result_df_sorted['City'])
ax.set_xlabel(f'Share of Population below {time_of_interest} mins travel time')
plt.xlim(0, 1)
plt.show()

# Convert and invert only numeric columns
result_df_sorted[result_df_sorted.columns.difference(['City'])] = result_df_sorted[result_df_sorted.columns.difference(['City'])].apply(pd.to_numeric, errors='coerce')
inverted_df = pd.concat([result_df_sorted[['City']], (1 - result_df_sorted[result_df_sorted.columns.difference(['City'])]) * 100], axis=1)
