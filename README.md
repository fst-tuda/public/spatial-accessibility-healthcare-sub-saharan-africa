# Spatial Accessibility to Healthcare in Sub Saharan Africa Repository

This repository contains Python scripts for analyzing population data over time, focusing on different aspects such as formal and informal areas, total population trends, and population ratios between specified times. The scripts utilize pandas, matplotlib, and numpy for data manipulation and visualization.

## File 1: `plot_SA_cities_with_landuse.py`

### Description
- Analyzes population data over time, focusing on formal and informal areas around specific hospitals.
- Calculates normalized populations and visualizes them with uncertainties.
- Generates CSV files with detailed population information.

### Usage
1. Update script variables such as `hospital_code`, `landuse`, and `time_of_interest` as needed.
`hospital_code` refers to the healthcare facilities considered. The possible variables are `C0` and `C2`. While `C2` consideres just hospitals, `C0` includes besides hospitals also smaller health care facilities.
2. Run the script using `python plot_SA_cities_with_landuse.py`.
3. View visualizations and find generated CSV files with population details.

## File 2: `plot_SA_cities_without_landuse.py`

### Description
- Explores total population trends over time for different cities.
- Calculates normalized populations with uncertainties.
- Generates visualizations and a DataFrame with population information.

### Usage
1. Update script variables like `hospital_code` and `time_of_interest` if necessary.
`hospital_code` refers to the healthcare facilities considered. The possible variables are `C0` and `C2`. While `C2` consideres just hospitals, `C0` includes besides hospitals also smaller health care facilities.
2. Execute the script using `python plot_SA_cities_without_landuse.py`.
3. Examine visualizations and review the resulting DataFrame for population details.

## File 3: `plot_relative_risk_cities.py`

### Description
- Performs a comparative analysis of normalized population ratios between formal and informal areas.
- Considers uncertainties in the population ratios.
- Generates CSV files and a plot illustrating the relative risk of population ratios.

### Usage
1. Update script variables like `hospital_code` and `time` as needed.
`hospital_code` refers to the healthcare facilities considered. The possible variables are `C0` and `C2`. While `C2` consideres just hospitals, `C0` includes besides hospitals also smaller health care facilities.
2. Run the script with `python plot_relative_risk_cities.py`.
3. Observe the generated CSV files and the plot showcasing the relative risk of population ratios.

## Dependencies
- Python 3.x
- pandas
- matplotlib
- numpy

## Results
- Visualizations, CSV files, and DataFrames are generated by the scripts, providing detailed insights into population trends and ratios.

## Notes
- Feel free to customize the scripts based on your specific data or analysis requirements.
- Make sure to install the required dependencies using `pip install -r requirements.txt` before running the scripts.

## Contributors
- Author: Friesen

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
