# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 10:25:05 2024

@author: Friesen
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Initialize variables
total_informal_population = 0
landuse = 'Formal' #'Informal' or 'Formal'
hospital_code = 'C2'
time_of_interest = 30 # in min
uncertainty = 0.2



# Variables
total_max_population = 0
total_population_lower_abs = 0

# Create a dictionary to store data for the specified time
specified_time_data = {'City': [], 'Normalized_Population': [], 'Population_Lower': [], 'Population_Upper': []}

# Read population data from a CSV file
df = pd.read_csv(f'Total_Population_{hospital_code}.csv')

# Load combined CSV file
combined_csv_path = f'combined_data_{hospital_code}.csv'
df_combined = pd.read_csv(combined_csv_path)

# Create a list to store dataframes for each city
city_data = []

# Iterate over unique cities in the DataFrame
unique_cities = df_combined['City'].unique()

# Process each city
for city_name in unique_cities:
    # Filter data for the current city and landuse
    data = df_combined[(df_combined['City'] == city_name) & (df_combined['Landuse'] == landuse)].sort_values(by=['Time'])

    # Calculate lower and upper boundaries for time
    data['time_lower'] = data['Time'] - uncertainty * data['Time']
    data['time_upper'] = data['Time'] + uncertainty * data['Time']

    # Calculate corresponding population for the lower and upper time bounds
    data['population_lower'] = np.interp(data['time_lower'], data['Time'], data['Total_Population'])
    data['population_upper'] = np.interp(data['time_upper'], data['Time'], data['Total_Population'])

    # Append the dataframe to the list
    city_data.append(data)

    # Calculate normalized population for the specified time
    max_population = df[(df['City'] == city_name) & (df['Landuse'] == landuse)]['Total_Population'].iloc[-1]
    normalized_population = data[data['Time'] == time_of_interest]['Total_Population'] / max_population
    population_lower = data[data['Time'] == time_of_interest]['population_lower'] / max_population
    population_upper = data[data['Time'] == time_of_interest]['population_upper'] / max_population
    population_lower_abs = data[data['Time'] == time_of_interest]['population_lower']
    population_upper_abs = data[data['Time'] == time_of_interest]['population_upper']

    # Store data in the dictionary
    specified_time_data['City'].append(data['City'].iloc[0])
    specified_time_data['Normalized_Population'].append(normalized_population.iloc[0])
    specified_time_data['Population_Lower'].append(population_lower.iloc[0])
    specified_time_data['Population_Upper'].append(population_upper.iloc[0])

    # Update total population variables
    total_informal_population += data[data['Time'] == time_of_interest]['Total_Population'].iloc[0]
    total_population_lower_abs += population_lower_abs.iloc[0]
    total_max_population += max_population

# Sort specified_time_data based on the 'Normalized_Population' column
sorted_specified_time_data = sorted(zip(specified_time_data['City'], specified_time_data['Normalized_Population'],
                                        specified_time_data['Population_Lower'], specified_time_data['Population_Upper']),
                                    key=lambda x: x[1], reverse=True)

# Unzip the sorted data
sorted_cities, sorted_normalized_pop, sorted_pop_lower, sorted_pop_upper = zip(*sorted_specified_time_data)

# Create population vs. time plot
plt.figure(figsize=(17, 7))
plt.subplot(121)
i = 1
for data in city_data:
    linestyle = '-' if i < 9 else '--' if i < 17 else ':'
    i += 1
    city_name = data['City'].iloc[1]
    filtered_df = df[(df['City'] == city_name) & (df['Landuse'] == landuse)]
    max_population = filtered_df['Total_Population'].iloc[-1]
    normalized_population = data['Total_Population'] / max_population
    plt.plot(data['Time'], normalized_population, label=data['City'].iloc[0], linestyle=linestyle)
    plt.fill_between(data['Time'], data['population_lower']/max_population, data['population_upper']/max_population, alpha=0.3)
plt.xlabel('Time (min)')
plt.xlim(0, 180)
plt.ylim(0, 1)
plt.ylabel('Normalized Population')
plt.title(f'Normalized {landuse} Population vs. Time with Uncertainty')
plt.legend(loc='upper left', bbox_to_anchor=(1, 1))



# Convert specified_time_data to a DataFrame
specified_time_df = pd.DataFrame(specified_time_data)

# Save DataFrame to a CSV file
csv_file_path = f'specified_time_data_{hospital_code}_{landuse}_{time_of_interest}_min.csv'
specified_time_df.to_csv(csv_file_path, index=False)
print(f'Data saved to {csv_file_path}')

# Sort DataFrame based on 'Normalized_Population'
result_df_sorted = specified_time_df.sort_values(by='Normalized_Population', ascending=True)

# Create a figure and axis
fig, ax = plt.subplots(figsize=(6, 6))

# Plot points for each city with error bars representing uncertainties
ax.errorbar(result_df_sorted['Normalized_Population'], range(len(result_df_sorted)),
            xerr=[result_df_sorted['Normalized_Population'] - result_df_sorted['Population_Lower'],
                  result_df_sorted['Population_Upper'] - result_df_sorted['Normalized_Population']],
            fmt='s', markersize=8, color='black', capsize=5)

# Add labels and title
ax.set_yticks(range(len(result_df_sorted)))
ax.set_yticklabels(result_df_sorted['City'])
ax.set_xlabel(f'Share of Population in {landuse} Areas below {time_of_interest} mins travel time')
plt.xlim(0, 1)
plt.show()

# Convert and invert only numeric columns
specified_time_df[specified_time_df.columns.difference(['City'])] = specified_time_df[specified_time_df.columns.difference(['City'])].apply(pd.to_numeric, errors='coerce')
inverted_df = pd.concat([specified_time_df[['City']], (1 - specified_time_df[specified_time_df.columns.difference(['City'])]) * 100], axis=1)
